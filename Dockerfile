FROM ubuntu:disco

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install python3 python3-pip curl -y
RUN apt-get autoclean
RUN apt-get clean
RUN pip3 install ansible
RUN pip3 install ansible-lint

RUN ansible-galaxy install -fciv f5devcentral.f5ansible